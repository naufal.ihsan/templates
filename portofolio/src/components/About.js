import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles(theme => ({
    root: {
        margin: "40px 0 0 0",
        fontFamily: "Lato",
        height: "80vh",
    },
    profpict: {
        marginTop: "50px",
        borderRadius: "10px",
        width: "300px",
        height: "300px",
    },
    text: {
        color: theme.palette.primary.contrastText,
        fontSize: "20px",
    },
    image: {
        width: "auto",
        height: "100px",
    },
    smallIntro: {
        color: '#03DAC6',
        fontSize: "20px",
        fontFamily: "B612 Mono",
    },
    intro: {
        color: "rgb(230, 241, 255)",
        fontWeight: "600",
        fontSize: "32px",
    },
}));

export default function About() {
    const classes = useStyles();

    return (
        <section id="about" className={classes.root}>
            <Grid container spacing={0}>
                <Grid item xs={8}>
                    <h2 className={classes.intro}><span className={classes.smallIntro}>01.</span> About Me</h2>
                    <p className={classes.text}>
                        Hello! I'm Brittany, a software engineer based in Jakarta <br />
                        who enjoys building things that live on the internet.<br />
                        I develop exceptional websites and web apps that provide intuitive,<br />
                        pixel-perfect user interfaces with efficient and modern backends.<br />
                    </p>
                    <p className={classes.text}>Here are a few technologies I've been working with recently:</p>
                    <Grid container spacing={0}>
                        <Grid item xs={3}>
                            <img className={classes.image} src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/200px-React-icon.svg.png" alt="react" />
                        </Grid>
                        <Grid item xs={3}>
                            <img className={classes.image} src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/96/Sass_Logo_Color.svg/200px-Sass_Logo_Color.svg.png" alt="sass" />
                        </Grid>
                        <Grid item xs={3}>
                            <img className={classes.image} src="https://upload.wikimedia.org/wikipedia/commons/2/21/Devicon-html5-plain-wordmark.svg" alt="html" />
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={4}>
                    <img className={classes.profpict} src="https://avatars0.githubusercontent.com/u/36224917?s=460&v=4" alt="profpict" />
                </Grid>
            </Grid>
        </section >
    );
}
