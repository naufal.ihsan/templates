import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Jumbotron from './Jumbotron';
import About from './About';
import Social from './Social';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        fontFamily: "B612 Mono",
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
}));

export default function FullWidthGrid() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Grid container spacing={0}>
                <Grid item xs={2}>
                    <Social />
                </Grid>
                <Grid item xs={8}>
                    <Jumbotron />
                    <About />
                </Grid>
                <Grid item xs={2}>

                </Grid>
            </Grid>
        </div>
    );
}