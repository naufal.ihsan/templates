import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
        fontFamily: "Lato",
    },
    icon1: {
        position: "fixed",
        top: "60vh",
        left: "5vw",
    },
    icon2: {
        position: "fixed",
        top: "68vh",
        left: "5vw",
    },
    icon3: {
        position: "fixed",
        top: "76vh",
        left: "5vw",
    },
    line: {
        content: "",
        display: "block",
        width: "1px",
        height: "90px",
        backgroundColor: "rgb(168, 178, 209)",
        margin: "0px auto",
    },
}));

export default function Social() {
    const classes = useStyles();

    return (
        <section id="social" className={classes.root}>
            <img className={classes.icon1} src="https://img.icons8.com/material/36/ffffff/twitter-squared.png" alt="twitter" />
            <img className={classes.icon2} src="https://img.icons8.com/material/36/ffffff/instagram.png" alt="ig" />
            <img className={classes.icon3} src="https://img.icons8.com/material/36/ffffff/linkedin.png" alt="linkedin" />
        </section>
    );
}
