import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import AccessibilityNewIcon from '@material-ui/icons/AccessibilityNew';
import { Link } from 'react-scroll'

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    btn: {
        textTransform: "unset!important",
        fontFamily: "B612 Mono",
        margin: "0 10px",
    },
    contrast: {
        color: '#03DAC6',
        margin: '0 8px',
    }
}));

export default function ButtonAppBar() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6" className={classes.title}>
                        <AccessibilityNewIcon />
                    </Typography>
                    <Link to="about" spy={true} smooth={true} duration={800}>
                        <Button className={classes.btn} color="inherit">
                            <span className={classes.contrast}>01.</span> About
                        </Button>
                    </Link>
                    <Button className={classes.btn} color="inherit">
                        <span className={classes.contrast}>02.</span> Experience
                    </Button>
                    <Button className={classes.btn} color="inherit">
                        <span className={classes.contrast}>03.</span> Work
                    </Button>
                    <Button className={classes.btn} color="inherit">
                        <span className={classes.contrast}>04.</span> Contact
                    </Button>
                    <Button className={classes.btn} color="secondary" variant="outlined">Resume</Button>
                </Toolbar>
            </AppBar>
        </div>
    );
}