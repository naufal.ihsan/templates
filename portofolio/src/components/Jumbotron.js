import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({
    root: {
        margin: "80px 0 0 0",
        fontFamily: "Lato",
        height: "80vh",
    },
    white: {
        color: theme.palette.primary.contrastText,
        fontSize: "20px",
    },
    btn: {
        textTransform: "unset!important",
        fontFamily: "B612 Mono",
        marginTop: "25px",
    },
    smallIntro: {
        color: '#03DAC6',
        fontSize: "24px",
        fontFamily: "B612 Mono",
        fontWeight: "normal",
        margin: "0px 0px 0px 0px;"
    },
    bigIntro: {
        color: "rgb(230, 241, 255)",
        fontSize: "72px",
        fontWeight: "600",
        margin: "0px 0px -10px 0px;"
    },
    bigIntroGray: {
        color: "rgb(136, 146, 176)",
        fontSize: "68px",
        fontWeight: "600",
        margin: "0px 0px 20px 3px;"
    }
}));

export default function Jumbotron() {
    const classes = useStyles();

    return (
        <section id="intro" className={classes.root}>
            <h2 className={classes.smallIntro}>Hi, my name is</h2>
            <h2 className={classes.bigIntro}>
                Naufal Ihsan .
            </h2>
            <h3 className={classes.bigIntroGray}>I
                build things for the web.
             </h3>
            <p className={classes.white}>
                I'm a software engineer based in Jakarta <br />
                specializing in building (and occasionally designing) exceptional, <br />
                high-quality websites and applications.
            </p>
            <Button className={classes.btn} color="secondary" variant="outlined">Get in touch</Button>
        </section >
    );
}
