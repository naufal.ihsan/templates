import React from 'react';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from "@material-ui/core/CssBaseline";
import ButtonAppBar from './components/Appbar';
import FullWidthGrid from './components/Layout';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#0A1930',
      contrastText: 'rgb(136, 146, 176)',
    },
    secondary: {
      main: '#03DAC6'
    },
    background: {
      default: '#0A1930',
    }
  }
});

function App() {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <div>
        <ButtonAppBar />
        <FullWidthGrid />
      </div>
    </ThemeProvider>
  );
}

export default App;
